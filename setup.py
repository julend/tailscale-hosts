from setuptools import setup, find_packages

setup(
    name="tailscale_hosts",
    version="0.1",
    packages=find_packages(),

    # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    install_requires=["elevate", "python_hosts"],

    # metadata to display on PyPI
    author="Jonathan Hamberg",
    author_email="jonathanhamberg@gmail.com",
    description="Automatically update hosts from tailscale peer hostnames.",
    keywords="tailscale,hosts",
    url="https://gitlab.com/jhamberg/tailscale-hosts",
    project_urls={
        "Source Code": "https://gitlab.com/jhamberg/tailscale-hosts",
    },
    classifiers=[
        "License :: OSI Approved :: Python Software Foundation License"
    ],
    entry_points={
        'console_scripts': [
            'tailscale_hosts=tailscale_hosts:main',
        ],
    }

    # could also include long_description, download_url, etc.
)
